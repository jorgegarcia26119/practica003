/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

/**
 *
 * @author _Enrique_
 */
public class Bomba {
    private int numBom;
    private  Gasolina gas;
    private float ventasTotales;
    
    public Bomba(){
        
        
    }
    public Bomba(Bomba bom){
        
        
    }
    
    public void setNumBom(int numBom){
        this.numBom = numBom;
    }
    
    public int getNumBom(){
        return numBom;
    }
        
    public void setGas(Gasolina gaso){
        this.gas = gaso;
    }
    
    public Gasolina getGas(){
        return gas;
    }
    
    public void iniciarBomba(int numbom, Gasolina gas){
        this.numBom = numbom;
        this.gas = gas;
        this.ventasTotales = 0;
    }
    
    public float venderGas(float cantidad){
        
         if (cantidad <= gas.getCantidad()) {
            float costo = cantidad * gas.getPrecio();
            gas.setCantidad(gas.getCantidad() - cantidad);
            ventasTotales += costo;
            return costo;
        } else {
            return 0;
        }
    }
    
     public void setVentasTotales(float ventasT) {
        this.ventasTotales = ventasT;
    }
     
     public float getVentasTotales() {
        return ventasTotales;
    }
}
