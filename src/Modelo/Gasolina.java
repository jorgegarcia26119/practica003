/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

/**
 *
 * @author _Enrique_
 */
public class Gasolina {
    
    private int idGas;
    private String tipo;
    private float precio;
    private float cant;
    
    public Gasolina(){}
    
    public Gasolina(Gasolina gas){
        this.idGas= gas.idGas;
        this.tipo = gas.tipo;
        this.precio = gas.precio;
        this.cant = gas.cant; 
    }
     
    public Gasolina(int id, String tipo, float precio) { 
        this.idGas = id;
        this.tipo = tipo;
        this.precio = precio;
        this.cant = 200;
    }
    
    public void setIdGas(int idGas){
        this.idGas = idGas;
    }
    
    public int getIdGas(){
        return idGas;
    }
    
     public void setTipo(String tipo){
        this.tipo = tipo;
    }
    
    public String getTipo(){
        return tipo;
    }
    
     public void setPrecio(float precio){
        this.precio = precio;
    }
    
    public float getPrecio(){
        return precio;
    }
    
    public void setCantidad(float cantidad) {
        this.cant = cantidad;
    }
    
    public float getCantidad() {
        return cant;
    }
     
}
